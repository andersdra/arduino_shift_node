#ifndef _ARDUINO_SHIFT_NODE_h
#define _ARDUINO_SHIFT_NODE_h

#define CHECKSUM 1
#define WATCHDOG_TIMEOUT WDTO_500MS

// uart
#define RX_BUF_SIZE 20
#define MASTER_RX_TIMEOUT_MS 5
// protocol
#define SR_SET 0
#define SR_MISC 1
#define RESET 0

// leds
#define LED_ON_STATE 1
#define NUMBER_OF_LEDS 6

#define TIMEOUT_LED 0
#define CHECKSUM_ERROR_LED 1
#define CHECKSUM_OK_LED 3
#define SET_SR_LED 5

static const uint8_t led_pin[NUMBER_OF_LEDS] = {14, 15, 16, 17, 18, 19};

uint8_t rx_index = 0;
uint8_t rx_buf_master[RX_BUF_SIZE];

typedef struct
{
  uint16_t on_ms = 0;
  uint16_t off_ms = 0;
  int8_t blinks = 0;
  uint8_t state = 0;
} status_t;

status_t led[NUMBER_OF_LEDS];

#endif
