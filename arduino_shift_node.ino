/*  32bit shift register node
    74HC595
    SER_IN  = D11, PB3
    L_CLOCK = D10, PB2
    CLOCK   = D13, PB5
    OE      = D12, PB4
    M_RESET>---/\/\/\--->+5V
             |   10K
             |
             D9, PB1
*/

#include "arduino_shift_node.h"
#include "shift.h"
#include "cobs.h"
#include <avr/wdt.h>
#include <avr/crc16.h>
#include <SPI.h>
#include <digitalWriteFast.h>
#include <elapsedMillis.h>

elapsedMillis master_rx;
elapsedMillis led_timer0;
elapsedMillis led_timer1;
elapsedMillis led_timer2;
elapsedMillis led_timer3;
elapsedMillis led_timer4;
elapsedMillis led_timer5;
elapsedMillis led_timer[NUMBER_OF_LEDS] = {led_timer0, led_timer1, led_timer2, led_timer3, led_timer4, led_timer5};

void leds_init()
{
  PORTC = 0;
  DDRC = 0x3f; // A0-A5
}

void setup()
{
  wdt_disable();
  leds_init();
  sr_init();
  Serial.begin(9600);
  wdt_enable(WATCHDOG_TIMEOUT);
  memset(rx_buf_master, 0, sizeof(rx_buf_master));
  rx_index = 0;
}

void loop()
{
  wdt_reset();
  process_master();
  do_leds();
}
// ALL 0: 1 1 1 1 1 3 30 88 0
// ALL 0xff: 1 7 ff ff ff ff c3 11 0

void process_master()
{
  if (rx_index > 0)
  {
    if (master_rx >= MASTER_RX_TIMEOUT_MS)
    {
      blink_led(TIMEOUT_LED, 2, 250, 250);
      memset(rx_buf_master, 0, sizeof(rx_buf_master));
      rx_index = 0;
      master_rx = 0;
    }
  }

  if (Serial.available() > 0)
  {
    uint8_t rxchar = Serial.read();

    if (rxchar != 0)
    {
      rx_buf_master[rx_index] = rxchar;
      rx_index++;
      master_rx = 0; // reset timer between each char
    }
    else
    {
      if (rx_index)
      {
        uint16_t rxcrc = 0xffff;
        uint8_t out_buffer[rx_index + 1];
        cobs_decode_result result;

        result = cobs_decode(out_buffer, (rx_index + 1), rx_buf_master, rx_index);

        uint16_t checksum = ((uint16_t)out_buffer[result.out_len - 2] << 8) | out_buffer[result.out_len - 1];
        for (uint8_t i = 0; i < result.out_len - 2; i++)
        {
          rxcrc = _crc_ccitt_update(rxcrc, out_buffer[i]);
        }

        uint8_t rx_id = out_buffer[0];

#if CHECKSUM
        if (checksum == rxcrc)
        {
          blink_led(CHECKSUM_OK_LED, 1, 100, 0);
#endif
          switch (rx_id)
          {
            case SR_SET:
              {
                blink_led(SET_SR_LED, 1, 200, 0);
                sr_32(out_buffer[1], out_buffer[2], out_buffer[3], out_buffer[4]);
                for (uint8_t i = 0; i < rx_index; i++)
                {
                  Serial.write(rx_buf_master[i]);
                }
                Serial.write((uint8_t)0);
              } break;

            case SR_MISC:
              {
                uint8_t rx_data = out_buffer[1];
                switch (rx_data)
                {
                  case RESET:
                    {
                      while (1) { // force WDT reset
                        do_leds();
                      };
                    } break;
                }
              } break;
          }
#if CHECKSUM
        }
        else // wrong checksum
        {
          blink_led(CHECKSUM_ERROR_LED, 2, 75, 75);
          // request new data
          Serial.println("checksumERR");
        }
#endif
        memset(rx_buf_master, 0, sizeof(rx_buf_master));
        rx_index = 0;
      }
    }
  }
}

void blink_led(uint8_t id, uint8_t blinks, uint16_t on_duration, uint16_t off_duration)
{
  digitalWriteFast(led_pin[id], LED_ON_STATE);
  led[id].on_ms = on_duration;
  led[id].off_ms = off_duration;
  led[id].blinks = blinks - 1;
  led[id].state = LED_ON_STATE;
  led_timer[id] = 0;
}

void do_leds()
{
  for (uint8_t i = 0; i < NUMBER_OF_LEDS; i++)
  {
    if (led[i].state != LED_ON_STATE && led[i].blinks > 0)
    {
      if (led_timer[i] >= led[i].off_ms)
      {
        digitalWriteFast(led_pin[i], LED_ON_STATE);
        led[i].blinks--;
        led[i].state = LED_ON_STATE;
        led_timer[i] = 0;
      }
    }
    else
    {
      if (led[i].blinks >= 0)
      {
        if (led_timer[i] >= led[i].on_ms)
        {
          digitalWriteFast(led_pin[i], !LED_ON_STATE);
          led[i].state = !LED_ON_STATE;
          led_timer[i] = 0;
          if (led[i].blinks == 0)
          {
            led[i].blinks = -1;
          }
        }
      }
    }
  }
}
