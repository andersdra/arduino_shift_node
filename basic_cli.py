#!/usr/bin/python3
import serial
import serial.tools.list_ports
from cmd import Cmd
from cobs import cobs
from time import sleep


ports = list(serial.tools.list_ports.comports())
for port in ports:
    if 'USB2.0-Serial' in port.description:
        print('node added: {}'.format(port.device))
        ser = serial.Serial(port.device, baudrate=9600, timeout=0.1, inter_byte_timeout=0.1, write_timeout=0.1)


def crc16_arr(bytelist):
    crc = 0xffff
    for a in range(len(bytelist)):
        crc ^= bytelist[a]
        for b in range(8):
            if (crc & 0x1):
                crc = (crc >> 1) ^ 0x8408
            else:
                crc = (crc >> 1)
    return crc


def crc16_update(crc, byte):
    crc ^= byte
    for a in range(8):
        if (crc & 0x1):
            crc = (crc >> 1) ^ 0x8408
        else:
            crc = (crc >> 1)
    return crc

# list of 4 bytes
def shift32(data):
    out = [0]
    crc = crc16_arr(out)
    for byte in data:
        out.append(byte)
        crc = crc16_update(crc, byte)
    out.append((crc >> 8) & 0xff)
    out.append(crc & 0xff)
    encoded = bytearray(cobs.encode(bytearray(out)))
    encoded.append(0)
    try:
        ser.write(encoded)
    except:
        print("node not connected?")
    rx = ser.read_until(b'\x00')
    if rx == encoded:
        for byte in data:
            print(bin(byte))
    else:
         print("tx vs rx does not match")


def shift32_hex(data):
    out = [0, (data >> 24) & 0xff, (data >> 16) & 0xff, (data >> 8) & 0xff, data & 0xff]
    crc = crc16_arr(out)
    out.append((crc >> 8) & 0xff)
    out.append(crc & 0xff)
    encoded = bytearray(cobs.encode(bytearray(out)))
    encoded.append(0)
    try:
        ser.write(encoded)
    except:
        print("node not connected?")
    rx = ser.read_until(b'\x00')
    if rx == encoded:
         print(bin(data))
    else:
         print("tx vs rx does not match")


def node_misc(id):
    crc = crc16_arr([1, id])
    ser.write(cobs.encode(bytearray([1, id, ((crc >> 8) & 0xff), crc & 0xff])) + b'\x00')


class node_prompt(Cmd):
    prompt = '> '
    intro = "shift node tester"

    def do_exit(self, input):
        ser.close()
        return True

    def do_misc(self, input):
        node_misc(int(input))

    def do_hex_shift(self, input):
        shift32_hex(int(input, 16))

    def do_flush_input(self, input):
        ser.reset_input_buffer()

    def do_set_register(self, input):
        integers = []
        for byte in input.split(','):
            integers.append(int(byte))
        shift32(integers)

    def do_read_node(self, input):
        rx = ser.read_until(b'\x00')
        print(rx)

    def default(self, input):
        if input == 'q':
            return self.do_exit(input)
        print("default: {}".format(input))

    do_EOF = do_exit


if __name__ == '__main__':
   node_prompt().cmdloop()
