#include "shift.h"
#include <digitalWriteFast.h>
#include <SPI.h>

void sr_init()
{
  digitalWriteFast(SR_OE_PIN, HIGH);
  digitalWriteFast(SR_MR_PIN, LOW);
  pinModeFast(SR_OE_PIN, OUTPUT);
  pinModeFast(SR_MR_PIN, OUTPUT);
  pinModeFast(SR_LATCH_CLOCK_PIN, OUTPUT);
  pinModeFast(SR_SER_IN_PIN, OUTPUT);
  sr_reset();
  SPI.beginTransaction(SPISettings(F_CPU, MSBFIRST, SPI_MODE0));
  SPI.begin();
  SPI.setClockDivider(SPI_CLOCK_DIV4);
  digitalWriteFast(SR_OE_PIN, LOW);
  digitalWriteFast(SR_MR_PIN, HIGH);
}

void sr_32(uint8_t sr0, uint8_t sr1, uint8_t sr2, uint8_t sr3)
{
  digitalWriteFast(SR_LATCH_CLOCK_PIN, LOW);
  SPI.transfer(sr0); // MSB bit7 == LED31
  SPI.transfer(sr1);
  SPI.transfer(sr2);
  SPI.transfer(sr3); // LSB bit0 == LED0
  digitalWriteFast(SR_LATCH_CLOCK_PIN, HIGH);
}

void sr_reset()
{
  digitalWriteFast(SR_OE_PIN, HIGH);
  digitalWriteFast(SR_MR_PIN, LOW);
  digitalWriteFast(SR_LATCH_CLOCK_PIN, LOW);
  digitalWriteFast(SR_SER_IN_PIN, LOW);
  digitalWriteFast(SR_LATCH_CLOCK_PIN, HIGH);
  digitalWriteFast(SR_SER_IN_PIN, HIGH);
  digitalWriteFast(SR_MR_PIN, HIGH);
  digitalWriteFast(SR_OE_PIN, LOW);
}
