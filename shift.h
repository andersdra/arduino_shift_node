#ifndef _SHIFT_h
#define _SHIFT_h

#include "Arduino.h"

#define SR_OE_PIN 12
#define SR_MR_PIN 9
#define SR_LATCH_CLOCK_PIN 10
#define SR_SER_IN_PIN 11

void sr_32(uint8_t, uint8_t, uint8_t, uint8_t);
void sr_reset();
void sr_init();

#endif
